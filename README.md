# Example Magento 2 extension

## Description
This extension shows the base extension setup for Magento 2.

### Prerequisites

```
Magento 2.3.2 (CE)
PHP 7.2
```

### Installation

#### Copy package files

- Download repository files as ZIP archive
- Extract files to the `app/code/Magebit/CodeSnippets` directory
- Run the following commands in Magento 2 root folder:

    ```
    php bin/magento module:enable Magebit_CodeSnippets
    ```
    
    ```
    php bin/magento setup:upgrade
    ```
    
    ```
    php bin/magento setup:di:compile
    ```
    
    ```
    php bin/magento setup:static-content:deploy
    ```

### Usage and Features
- There is possible to see 'Hello World!' text in 'Content -> CodeSnippet -> Hello World' https://prnt.sc/qwrs9q

## Authors

* **Vladislavs Onufrijevs**