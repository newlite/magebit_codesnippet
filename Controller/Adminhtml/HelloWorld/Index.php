<?php
/**
 * Magebit_CodeSnippet
 *
 * @category  Magebit
 * @package   Magebit_CodeSnippet
 * @author    Vladislavs Onufrijevs <vladislavs.onufrijevs@magebit.com>
 * @copyright 2020 [Magebit, Ltd.(http://www.magebit.com/)]
 */
declare(strict_types=1);

namespace Magebit\CodeSnippet\Controller\Adminhtml\HelloWorld;

/**
 * Class Index
 * @package Magebit\CodeSnippet\Controller\Adminhtml\HelloWorld
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/codesnippet_helloworld_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return  $resultPage = $this->resultPageFactory->create();
    }
}
